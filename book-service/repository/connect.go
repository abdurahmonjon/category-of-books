package repository

import (
	"cat-book/config"
	"fmt"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

func connect(cfg config.PostgresConfig) (*sqlx.DB, error) {
	db, err := sqlx.Connect(
		"postgres",
		fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable",
			cfg.PostgresHost, cfg.PostgresPort, cfg.PostgresUser, cfg.PostgresDB,
		),
	)

	if err != nil {
		return nil, err
	}

	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	if err != nil {
		return nil, err
	}
	m, err := migrate.NewWithDatabaseInstance(fmt.Sprintf("file://%s", cfg.PostgresMigrationsPath), "postgres", driver)
	if err != nil {
		return nil, err
	}

	if err = m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
		return nil, err
	}

	return db, nil
}

//package repository
//
//import (
//	"cat-book/config"
//	"errors"
//	"fmt"
//	"github.com/golang-migrate/migrate/v4"
//	"log"
//
//	"github.com/golang-migrate/migrate/v4/database/postgres"
//	_ "github.com/golang-migrate/migrate/v4/source/file"
//	"github.com/jmoiron/sqlx"
//	_ "github.com/lib/pq"
//)
//
//func connect(cfg config.PostgresConfig) (*sqlx.DB, error) {
//	db, err := sqlx.Connect(
//		"postgres",
//		fmt.Sprintf(
//			"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
//			cfg.PostgresHost, cfg.PostgresPort, cfg.PostgresUser, cfg.PostgresPassword, cfg.PostgresDB,
//		),
//	)
//	if err != nil {
//		log.Println("error driver setting")
//		return nil, err
//	}
//
//	driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
//	if err != nil {
//		return nil, err
//	}
//
//	m, err := migrate.NewWithDatabaseInstance(fmt.Sprintf("file://%s", cfg.PostgresMigrationsPath), "postgres", driver)
//	if err != nil {
//		log.Println("error migrate file")
//		return nil, err
//	}
//
//	if err = m.Up(); err != nil && !errors.Is(err, migrate.ErrNoChange) {
//		log.Println("error up", err)
//		return nil, err
//	}
//
//	return db, nil
//}
