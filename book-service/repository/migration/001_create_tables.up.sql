create table authors (
    id uuid not null primary key,
    first_name varchar(55) not null,
    last_name varchar(55) not null
);
create table bookcategories (
    id uuid not null primary key,
    kind varchar(255) not null
);
create table books (
    id uuid not null primary key,
    title varchar(55) not null,
    author_id uuid not null references authors(id),
    category_id uuid not null references bookcategories(id)
);

