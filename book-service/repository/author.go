package repository

import "github.com/google/uuid"

type Author struct {
	Id        uuid.UUID `db:"author_id"`
	FirstName string    `db:"first_name"`
	LastName  string    `db:"last_name"`
}
