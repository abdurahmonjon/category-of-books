package repository

import (
	"cat-book/config"
	"cat-book/domain/authors"
	"cat-book/domain/books"
	"cat-book/domain/categories"
	"cat-book/pkg/errs"
	"context"
	"database/sql"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
	"log"
)

type Postgres struct {
	db *sqlx.DB
}

func New(cfg config.PostgresConfig) (*Postgres, error) {
	db, err := connect(cfg)
	if err != nil {
		log.Println("error1: ", err)
		return &Postgres{}, err
	}
	return &Postgres{
		db: db,
	}, nil
}

// Postgres implements service.RepositoryInterface interface

func (p *Postgres) CreateAuthor(ctx context.Context, author authors.Author) error {
	return p.createAuthor(ctx, convertToPostgresAuthor(author))
}
func (p *Postgres) createAuthor(ctx context.Context, author Author) error {
	query := `insert into authors values ($1, $2, $3)`
	_, err := p.db.ExecContext(ctx, query, author.Id, author.FirstName, author.LastName)
	return err
}

func (p *Postgres) CreateBook(ctx context.Context, book books.Book) error {
	return p.createBook(ctx, convertToPostgresBook(book))
}
func (p *Postgres) createBook(ctx context.Context, book Book) error {
	query := `insert into books values ($1, $2, $3, $4)`
	//log.Println("p book", book)
	_, err := p.db.ExecContext(ctx, query, book.Id, book.Title, book.AuthorId, book.CategoryId)
	//log.Println(err)
	return err
}

func (p *Postgres) CreateCategory(ctx context.Context, category categories.Category) error {
	return p.createCategory(ctx, convertToPostgresCategory(category))
}
func (p *Postgres) createCategory(ctx context.Context, category Category) error {
	query := `insert into bookcategories values ($1, $2)`
	_, err := p.db.ExecContext(ctx, query, category.Id, category.Kind)
	//log.Println("execute err", err)
	return err
}

func (p *Postgres) GetBook(ctx context.Context, id uuid.UUID) (books.Book, error) {
	b, err := p.getBook(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return books.Book{}, errs.ErrNotFound
		}
		return books.Book{}, err
	}
	return books.UnmarshalBook(books.UnmarshalBookArgs(b))
}
func (p *Postgres) getBook(ctx context.Context, id uuid.UUID) (Book, error) {
	query := `select * from books where id = $1`
	var b Book
	if err := p.db.GetContext(ctx, &b, query, id); err != nil {
		return Book{}, nil
	}
	return b, nil
}
func (p *Postgres) GetAllBooks(ctx context.Context) ([]books.Book, error) {
	b, err := p.getAllBooks(ctx)
	if err != nil {
		log.Println("1:", err)
	}
	newB := converter(b)
	return newB, err
}
func (p *Postgres) getAllBooks(ctx context.Context) ([]Book, error) {
	query := `select * from books`
	bs := make([]Book, 0)
	if err := p.db.SelectContext(ctx, &bs, query); err != nil {
		return nil, err
	}
	return bs, nil
}

func (p *Postgres) UpdateBook(ctx context.Context, book books.Book) error {
	return p.updateBook(ctx, book)
}

func (p *Postgres) updateBook(ctx context.Context, book books.Book) error {
	query := `update books set title=$1,author_id=$2,category_id=$3`
	if _, err := p.db.ExecContext(ctx, query, book.Title(), book.Author(), book.Category()); err != nil {
		return err
	}
	return nil
}
func (p *Postgres) DeleteBook(ctx context.Context, id uuid.UUID) error {
	return p.deleteBook(ctx, id)
}
func (p Postgres) deleteBook(ctx context.Context, id uuid.UUID) error {
	query := `delete from books where id = $1`
	_, err := p.db.ExecContext(ctx, query, id)
	return err

}
func converter(book []Book) []books.Book {
	newBooks := make([]books.Book, 0)
	for _, i := range book {
		newBook, err := books.UnmarshalBook(books.UnmarshalBookArgs(i))
		if err != nil {
			log.Println("marshaliiiiiiiing:", err)
			return nil
		}
		newBooks = append(newBooks, newBook)
	}
	return newBooks
}
