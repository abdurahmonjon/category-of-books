package repository

import "github.com/google/uuid"

type Book struct {
	Id         uuid.UUID `db:"id"`
	Title      string    `db:"title"`
	AuthorId   uuid.UUID `db:"author_id"`
	CategoryId uuid.UUID `db:"category_id"`
}
