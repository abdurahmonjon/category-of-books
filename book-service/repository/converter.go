package repository

import (
	"cat-book/domain/authors"
	"cat-book/domain/books"
	"cat-book/domain/categories"
)

func convertToPostgresAuthor(a authors.Author) Author {
	return Author{
		Id:        a.Id(),
		FirstName: a.FirstName(),
		LastName:  a.LastName(),
	}
}
func convertToPostgresBook(b books.Book) Book {
	return Book{
		b.Id(),
		b.Title(),
		b.Author(),
		b.Category(),
	}
}

func convertToPostgresCategory(c categories.Category) Category {
	//log.Println("convertingcategory:", c)
	return Category{
		c.Id(),
		c.Kind(),
	}
}
