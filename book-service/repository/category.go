package repository

import "github.com/google/uuid"

type Category struct {
	Id   uuid.UUID `db:"id"`
	Kind string    `db:"kind"`
}
