package main

import (
	"cat-book/config"
	"cat-book/domain/authors"
	"cat-book/domain/books"
	"cat-book/domain/categories"
	"cat-book/pkg/id"
	"cat-book/protos"
	"cat-book/repository"
	"cat-book/server"
	"cat-book/service"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	cfg, err := config.Load()
	if err != nil {
		panic(err)
	}
	repo, err := repository.New(cfg.PostgresConfig)
	if err != nil {
		log.Fatal(err)
	}
	bookFactory := books.NewFactory(id.Generator{})
	authorFactory := authors.NewFactory(id.Generator{})
	categoryFactory := categories.NewFactory(id.Generator{})

	svc := service.New(repo, bookFactory, authorFactory, categoryFactory)
	server := server.New(svc, bookFactory, authorFactory, categoryFactory)

	lis, err := net.Listen("tcp", net.JoinHostPort(cfg.Host, cfg.Port))
	if err != nil {
		panic(err)
	}
	grpcServer := grpc.NewServer()
	protos.RegisterBookServiceServer(grpcServer, server)
	if err := grpcServer.Serve(lis); err != nil {
		panic(err)
	}
}
