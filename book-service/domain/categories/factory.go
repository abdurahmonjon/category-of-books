package categories

import "cat-book/pkg/id"

type Factory struct {
	idGenerator id.Generator
}

func NewFactory(id id.Generator) Factory {
	return Factory{
		idGenerator: id,
	}
}

// NewCategory is a constructor that checks if the provided data for categorypb is valid or not
// new categorypb objects can only be created through this constructor which ensures everything is valid
func (f Factory) NewCategory(kind string) (Category, error) {
	c := Category{
		id:   f.idGenerator.GenerateUUID(),
		kind: kind,
	}
	if err := c.validate(); err != nil {
		return Category{}, err
	}
	return c, nil
}
