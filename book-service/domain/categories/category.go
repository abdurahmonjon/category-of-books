package categories

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

var (
	InvalidCategoryData = errors.New("invalid categorypb data")
)

// Category represents domain object that holds required info for a Category
// All core business logic relevant to categories should be done through this struct
type Category struct {
	id   uuid.UUID
	kind string
}

func (c Category) Id() uuid.UUID {
	return c.id
}
func (c Category) Kind() string {
	return c.kind
}

func (c Category) validate() error {
	if c.kind == "" {
		errors.Errorf("%w kind of categorypb is required", InvalidCategoryData)
	}
	return nil
}
