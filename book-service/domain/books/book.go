package books

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

var (
	InvalidBookData = errors.New("invalid bookpb.proto data")
)

// Book represents domain object that holds required info for a bookpb.proto
// All core business logic relevant to books should be done through this struct
type Book struct {
	id         uuid.UUID
	title      string
	authorId   uuid.UUID
	categoryId uuid.UUID
}

func (b Book) Id() uuid.UUID {
	return b.id
}
func (b Book) Title() string {
	return b.title
}
func (b Book) Author() uuid.UUID {
	return b.authorId
}
func (b Book) Category() uuid.UUID {
	return b.categoryId
}
func (b Book) validate() error {
	if uuid.UUID.String(b.authorId) == "" {
		errors.Errorf("%w Author Name must be set", InvalidBookData)
	}
	if uuid.UUID.String(b.categoryId) == "" {
		errors.Errorf("%w Category Name must be set", InvalidBookData)
	}
	return nil
}

type UnmarshalBookArgs struct {
	Id         uuid.UUID
	Title      string
	AuthorId   uuid.UUID
	CategoryId uuid.UUID
}

func UnmarshalBook(args UnmarshalBookArgs) (Book, error) {
	b := Book{
		id:         args.Id,
		title:      args.Title,
		authorId:   args.AuthorId,
		categoryId: args.CategoryId,
	}
	if err := b.validate(); err != nil {
		return Book{}, err
	}
	return b, nil
}
