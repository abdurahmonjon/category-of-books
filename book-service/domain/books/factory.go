package books

import (
	"cat-book/pkg/id"
	"github.com/google/uuid"
	"log"
)

type Factory struct {
	idGenerator id.Generator
}

func NewFactory(idGenerator id.Generator) Factory {
	return Factory{
		idGenerator: idGenerator,
	}
}

// NewBook is a constructor that checks if the provided data for Book is valid or not
// new Book objects can only be created through this constructor which ensures everything is valid
func (f Factory) NewBook(title string, author, category uuid.UUID) (Book, error) {
	b := Book{
		id:         f.idGenerator.GenerateUUID(),
		title:      title,
		authorId:   author,
		categoryId: category,
	}
	log.Println(b)
	if err := b.validate(); err != nil {
		log.Println("err validate:")
		return Book{}, err
	}
	log.Println("b:", b)
	return b, nil

}
