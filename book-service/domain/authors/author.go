package authors

import (
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

var (
	InvalidAuthorDate = errors.New("InvalidAuthorDate")
)

// Author represents domain object that holds required info for an Author
// All core business logic relevant to Author should be done through this struct
type Author struct {
	id        uuid.UUID
	firstName string
	lastName  string
}

func (a Author) Id() uuid.UUID {
	return a.id
}
func (a Author) FirstName() string {
	return a.firstName
}
func (a Author) LastName() string {
	return a.lastName
}
func (a Author) validate() error {
	if a.firstName == "" {
		return errors.Errorf("%w FirstName must be set", InvalidAuthorDate)
	}
	if a.lastName == "" {
		return errors.Errorf("%w LastName must be set", InvalidAuthorDate)
	}
	return nil
}
