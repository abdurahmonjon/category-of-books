package authors

import "cat-book/pkg/id"

type Factory struct {
	idGenerator id.Generator
}

func NewFactory(idGenerator id.Generator) Factory {
	return Factory{
		idGenerator: idGenerator,
	}

}

// NewAuthor is a constructor that checks if the provided data for Author is valid or not
// new Author objects can only be created through this constructor which ensures everything is valid
func (f Factory) NewAuthor(firstName, lastName string) (Author, error) {
	a := Author{
		id:        f.idGenerator.GenerateUUID(),
		firstName: firstName,
		lastName:  lastName,
	}
	if err := a.validate(); err != nil {
		return Author{}, err
	}
	return a, nil
}
