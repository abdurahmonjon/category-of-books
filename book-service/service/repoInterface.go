package service

import (
	"cat-book/domain/authors"
	"cat-book/domain/books"
	"cat-book/domain/categories"
	"context"
	"github.com/google/uuid"
)

type RepositoryInterface interface {
	CreateAuthor(ctx context.Context, author authors.Author) error
	CreateBook(ctx context.Context, book books.Book) error
	CreateCategory(ctx context.Context, category categories.Category) error
	GetBook(ctx context.Context, id uuid.UUID) (books.Book, error)
	GetAllBooks(ctx context.Context) ([]books.Book, error)
	UpdateBook(ctx context.Context, book books.Book) error
	DeleteBook(ctx context.Context, id uuid.UUID) error
}
