package service

import (
	"cat-book/domain/authors"
	"cat-book/domain/books"
	"cat-book/domain/categories"
	"context"
	"github.com/google/uuid"
	"log"
)

type Service struct {
	repo            RepositoryInterface
	bookFactory     books.Factory
	authorFactory   authors.Factory
	categoryFactory categories.Factory
}

func New(repo RepositoryInterface, bookFactory books.Factory, authorFactory authors.Factory, categoryFactory categories.Factory) Service {
	return Service{
		repo:            repo,
		bookFactory:     bookFactory,
		authorFactory:   authorFactory,
		categoryFactory: categoryFactory,
	}
}
func (s Service) RegisterBook(ctx context.Context, book books.Book) (books.Book, error) {
	err := s.repo.CreateBook(ctx, book)
	if err != nil {
		return books.Book{}, err
	}
	return book, nil
}

func (s Service) RegisterAuthor(ctx context.Context, author authors.Author) (authors.Author, error) {
	err := s.repo.CreateAuthor(ctx, author)
	if err != nil {
		return authors.Author{}, err
	}
	return author, nil
}
func (s Service) RegisterCategory(ctx context.Context, category categories.Category) (categories.Category, error) {
	err := s.repo.CreateCategory(ctx, category)
	log.Println("repo category:", category)
	if err != nil {
		//log.Println("111", err)
		return categories.Category{}, err
	}
	//log.Println("111:", category)
	return category, nil
}
func (s Service) GetBook(ctx context.Context, id uuid.UUID) (books.Book, error) {
	book, err := s.repo.GetBook(ctx, id)
	if err != nil {
		return books.Book{}, err
	}
	return book, nil
}
func (s Service) GetAllBooks(ctx context.Context) ([]books.Book, error) {
	books, err := s.repo.GetAllBooks(ctx)
	if err != nil {
		log.Println("err2", err)
		return nil, err
	}
	return books, nil
}
func (s Service) UpdateBook(ctx context.Context, book books.Book) error {
	err := s.repo.UpdateBook(ctx, book)
	return err
}
func (s Service) DeleteBook(ctx context.Context, id uuid.UUID) error {
	err := s.repo.DeleteBook(ctx, id)
	return err
}
