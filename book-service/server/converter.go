package server

import (
	"cat-book/domain/authors"
	"cat-book/domain/books"
	"cat-book/domain/categories"
	"cat-book/protos"
)

func toProtoBooks(book []books.Book) *protos.Books {
	var protoBooks []*protos.Book
	for _, i := range book {
		protoBooks = append(protoBooks, toProtoBook(i))
	}
	return &protos.Books{Books: protoBooks}
}
func toProtoBook(book books.Book) *protos.Book {
	//log.Println(">>>>>>>>>>>>>>>>>>:", book.Id(), book.Title())
	return &protos.Book{
		Id:       book.Id().String(),
		Title:    book.Title(),
		Author:   book.Author().String(),
		Category: book.Category().String(),
	}
}
func toProtoAuthor(author authors.Author) *protos.Author {
	return &protos.Author{
		Id:        author.Id().String(),
		FirstName: author.FirstName(),
		LastName:  author.LastName(),
	}
}
func toProtoCategory(category categories.Category) *protos.Category {
	return &protos.Category{
		Id:   category.Id().String(),
		Kind: category.Kind(),
	}
}
