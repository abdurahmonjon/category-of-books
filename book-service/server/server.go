package server

import (
	"cat-book/domain/authors"
	"cat-book/domain/books"
	"cat-book/domain/categories"
	"cat-book/protos"
	"cat-book/service"
	"context"
	"github.com/google/uuid"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
)

type Server struct {
	protos.UnimplementedBookServiceServer
	service  service.Service
	book     books.Factory
	author   authors.Factory
	category categories.Factory
}

func New(service service.Service, book books.Factory, author authors.Factory, category categories.Factory) Server {
	return Server{
		service:  service,
		book:     book,
		author:   author,
		category: category,
	}
}
func (s Server) RegisterBook(ctx context.Context, req *protos.RegisterBookRequest) (*protos.Book, error) {
	newBook, err := s.convertRequestBookToDomain(req)
	//log.Println(newBook)
	if err != nil {
		return nil, err
	}
	createdBook, err2 := s.service.RegisterBook(ctx, newBook)
	if err2 != nil {
		return nil, err2
	}
	//log.Println(toProtoBook(createdBook))
	return toProtoBook(createdBook), nil
}

func (s Server) RegisterAuthor(ctx context.Context, req *protos.RegisterAuthorRequest) (*protos.Author, error) {
	newAuthor, err := s.convertRequestAuthorToDomain(req)
	//log.Println("newAuthor:", newAuthor)
	//log.Println("request:", req)
	if err != nil {
		return nil, err
	}
	createdAuthor, err2 := s.service.RegisterAuthor(ctx, newAuthor)
	if err2 != nil {
		return nil, err2
	}
	return toProtoAuthor(createdAuthor), nil

}
func (s Server) RegisterCategory(ctx context.Context, req *protos.RegisterCategoryRequest) (*protos.Category, error) {
	newCategory, err := s.convertRequestCategoryToDomain(req)
	//log.Println("newcategory", newCategory)
	//log.Println("category req", req)
	if err != nil {
		return nil, err
	}
	createdCategory, err2 := s.service.RegisterCategory(ctx, newCategory)
	//log.Println(createdCategory, err2)
	if err2 != nil {
		return nil, err2
	}
	//log.Println("created category:", createdCategory)
	return toProtoCategory(createdCategory), nil
}
func (s Server) GetBook(ctx context.Context, req *protos.GetBookRequest) (*protos.Book, error) {
	id, err := uuid.Parse(req.Id)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "id is not uuid")
	}
	book, err := s.service.GetBook(ctx, id)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return toProtoBook(book), nil
}

func (s Server) GetBooks(ctx context.Context, empty *emptypb.Empty) (*protos.Books, error) {
	log.Println(11)
	books, err := s.service.GetAllBooks(ctx)
	if err != nil {
		log.Println("err1", err)
		return nil, status.Error(codes.Internal, err.Error())
	}
	return toProtoBooks(books), nil
}
func (s Server) UpdateBook(ctx context.Context, req *protos.UpdateBookRequest) (*protos.Book, error) {
	book, err := s.convertUpdateRequestBookToDomain(req)
	if err != nil {
		return nil, err
	}
	err = s.service.UpdateBook(ctx, book)
	if err != nil {
		return nil, err
	}
	return toProtoBook(book), nil
}

func (s Server) DeleteBook(ctx context.Context, req *protos.DeleteBookRequest) (*emptypb.Empty, error) {
	bookId, err := uuid.Parse(req.Id)
	if err != nil {
		log.Println(err, "ddd")
		return &emptypb.Empty{}, err
	}
	err = s.service.DeleteBook(ctx, bookId)
	if err != nil {
		return &emptypb.Empty{}, err
	}
	return &emptypb.Empty{}, nil
}

func (s Server) convertUpdateRequestBookToDomain(book *protos.UpdateBookRequest) (books.Book, error) {
	authorId, err := uuid.Parse(book.Author)
	if err != nil {
		return books.Book{}, status.Error(codes.InvalidArgument, "author id is not uuid")
	}
	categoryId, err := uuid.Parse(book.Category)
	if err != nil {
		return books.Book{}, status.Error(codes.InvalidArgument, "category id is not uuid")
	}
	bookId, err := uuid.Parse(book.GetId())
	if err != nil {
		return books.Book{}, err
	}
	unmarshalledBook := books.UnmarshalBookArgs{
		Id:         bookId,
		Title:      book.Title,
		AuthorId:   authorId,
		CategoryId: categoryId,
	}
	return books.UnmarshalBook(unmarshalledBook)
}

//converter converts requestBook to domain book struct
func (s Server) convertRequestBookToDomain(protoBook *protos.RegisterBookRequest) (books.Book, error) {
	//log.Println("protobook:", protoBook)
	authorId, err := uuid.Parse(protoBook.Author)
	//log.Println("authorid", authorId)
	if err != nil {
		return books.Book{}, status.Error(codes.InvalidArgument, "authorId is not uuid ")
	}
	categoryId, err2 := uuid.Parse(protoBook.Category)
	//log.Println("categoryid", categoryId)
	if err2 != nil {
		return books.Book{}, status.Error(codes.InvalidArgument, "categoryId is not uuid ")
	}
	newBook, err := s.book.NewBook(
		protoBook.Title,
		authorId,
		categoryId)
	//log.Println("newbook:", newBook)
	return newBook, nil
}

//converter converts authorRequest to author domain struct
func (s Server) convertRequestAuthorToDomain(protoAuthor *protos.RegisterAuthorRequest) (authors.Author, error) {
	newAuthor, err := s.author.NewAuthor(
		protoAuthor.FirstName,
		protoAuthor.LastName)
	//log.Println("first", protoAuthor.FirstName, protoAuthor.LastName)
	if err != nil {
		return authors.Author{}, err
	}
	return newAuthor, nil
}
func (s Server) convertRequestCategoryToDomain(protoCategory *protos.RegisterCategoryRequest) (categories.Category, error) {
	newCategory, err := s.category.NewCategory(protoCategory.Kind)
	if err != nil {
		return categories.Category{}, err
	}
	return newCategory, nil
}
