package adapter

import (
	"api-gateway/protos"
	"api-gateway/request"
	"api-gateway/responce"
	"context"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
)

func NewBookService(client protos.BookServiceClient) BookService {
	return BookService{
		client: client,
	}
}

type BookService struct {
	client protos.BookServiceClient
}

func (b BookService) CreateBook(ctx context.Context, req request.RegisterBookRequest) (responce.RegisterBookResponce, error) {
	grpcRequest := &protos.RegisterBookRequest{
		Title:    req.Title,
		Author:   req.Author,
		Category: req.Category,
	}
	//log.Println("grpc", grpcRequest)
	res, err := b.client.RegisterBook(ctx, grpcRequest)
	//log.Println("res: ", res)
	if err != nil {
		return responce.RegisterBookResponce{}, err
	}
	//log.Println(res)
	return responce.RegisterBookResponce{
		Id:       res.Id,
		Title:    res.Title,
		Author:   res.Author,
		Category: res.Category,
	}, nil
}
func (b BookService) CreateAuthor(ctx context.Context, req request.RegisterAuthorRequest) (responce.RegisterAuthorResponse, error) {
	grpcRequest := &protos.RegisterAuthorRequest{
		FirstName: req.FirstName,
		LastName:  req.LastName,
	}
	//log.Println(grpcRequest)
	res, err := b.client.RegisterAuthor(ctx, grpcRequest)
	//log.Println(res)
	if err != nil {
		return responce.RegisterAuthorResponse{}, err
	}
	return responce.RegisterAuthorResponse{
		Id:        res.Id,
		FirstName: res.FirstName,
		LastName:  res.LastName,
	}, nil
}
func (b BookService) CreateCategory(ctx context.Context, req request.RegisterCategoryRequest) (responce.RegisterCategoryResponse, error) {
	grpcRequest := &protos.RegisterCategoryRequest{
		Kind: req.Kind,
	}
	//log.Println("create category grpc:", grpcRequest)
	res, err := b.client.RegisterCategory(ctx, grpcRequest)
	//log.Println("category responce:", res)
	//log.Println("res.kind", res.Kind)
	//log.Println("res.id", res.Id)
	if err != nil {
		return responce.RegisterCategoryResponse{}, err
	}
	return responce.RegisterCategoryResponse{
		ID:   res.Id,
		Kind: res.Kind,
	}, nil
}
func (b BookService) GetAllBooks(ctx context.Context) ([]responce.RegisterBookResponce, error) {
	books, err := b.client.GetBooks(ctx, &emptypb.Empty{})
	if err != nil {
		log.Println("error1:", err)
		return nil, err
	}
	return protoBooksToResponseBooks(books), nil
}
func (b BookService) GetBook(ctx context.Context, id string) (responce.RegisterBookResponce, error) {
	res, err := b.client.GetBook(ctx, &protos.GetBookRequest{Id: id})
	if err != nil {
		return responce.RegisterBookResponce{}, nil
	}
	return responce.RegisterBookResponce{
		Id:       res.Id,
		Title:    res.Title,
		Author:   res.Author,
		Category: res.Category,
	}, nil
}
func (b BookService) UpdateBook(ctx context.Context, req request.UpdateBookRequest) (responce.RegisterBookResponce, error) {
	grpcRequest := &protos.UpdateBookRequest{
		Id:       req.Id,
		Title:    req.Title,
		Author:   req.Author,
		Category: req.Category,
	}
	res, err := b.client.UpdateBook(ctx, grpcRequest)
	if err != nil {
		return responce.RegisterBookResponce{}, nil
	}
	return responce.RegisterBookResponce{
		Id:       res.Id,
		Title:    res.Title,
		Author:   res.Author,
		Category: res.Category,
	}, nil
}

func (b BookService) DeleteBook(ctx context.Context, id string) error {
	_, err := b.client.DeleteBook(ctx, &protos.DeleteBookRequest{Id: id})
	log.Println("sdfghjk", err, "err")
	return err
}

func protoBooksToResponseBooks(bProto *protos.Books) []responce.RegisterBookResponce {
	var books []responce.RegisterBookResponce
	for _, b := range bProto.Books {
		books = append(books, responce.RegisterBookResponce{
			Id:       b.GetId(),
			Title:    b.GetTitle(),
			Author:   b.GetAuthor(),
			Category: b.GetCategory(),
		})
	}
	return books

}
