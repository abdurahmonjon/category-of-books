package main

import (
	"api-gateway/adapter"
	"api-gateway/client/grpc"
	"api-gateway/handler"
	"api-gateway/service"
	"context"
	"github.com/gin-gonic/gin"
	_ "github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"net/http"
	"time"
)

const bookServiceClient = "localhost: 7777"

// @title          cat-Book
// @version         1.0
// @description      Simple microservice for catigories of book.

// NewRouter
// @contact.name   API Support
// @contact.url    http://www.swagger.io/support
// @contact.email  support@swagger.io

func main() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	bookServiceClient, err := grpc.NewBookServiceClient(ctx, bookServiceClient)
	if err != nil {
		panic("failed to create book service client")
	}
	bookService := adapter.NewBookService(bookServiceClient)
	service := service.NewService(bookService)

	h := handler.New(service)

	g := gin.Default()
	g.POST("/register-book", h.RegisterBook)
	g.POST("/register-author", h.RegisterAuthor)
	g.POST("/register-category", h.RegisterCategory)
	g.GET("/get-book/:id", h.GetBook)
	g.GET("/get-books", h.GetAllBooks)
	g.PUT("/update-book", h.UpdateBook)
	g.DELETE("/delete-book/:id", h.DeleteBook)

	g.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	http.ListenAndServe(":7070", g)
}
