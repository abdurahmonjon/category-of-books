package service

import (
	"api-gateway/request"
	"api-gateway/responce"
	"context"
)

type Service struct {
	Book BookServiceClient
}

func NewService(bookService BookServiceClient) Service {
	return Service{
		Book: bookService,
	}
}

type BookServiceClient interface {
	CreateBook(context.Context, request.RegisterBookRequest) (responce.RegisterBookResponce, error)
	GetBook(ctx context.Context, id string) (responce.RegisterBookResponce, error)
	GetAllBooks(ctx context.Context) ([]responce.RegisterBookResponce, error)
	UpdateBook(ctx context.Context, book request.UpdateBookRequest) (responce.RegisterBookResponce, error)
	DeleteBook(ctx context.Context, id string) error

	CreateAuthor(context.Context, request.RegisterAuthorRequest) (responce.RegisterAuthorResponse, error)
	CreateCategory(context.Context, request.RegisterCategoryRequest) (responce.RegisterCategoryResponse, error)
}
