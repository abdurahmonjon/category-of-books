package handler

import (
	_ "api-gateway/docs"
	"api-gateway/request"
	"api-gateway/service"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
)

func New(svc service.Service) Handler {
	return Handler{
		service: svc,
	}
}

type Handler struct {
	service service.Service
}

// RegisterBook
// @Summary      RegisterBook
// @Description  registers a book
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        request body request.RegisterBookRequest true "book registered"
// @Success      200 {object} responce.RegisterBookResponce
// @Failure      400
// @Failure      500
// @Router       /register-book [POST]
func (h Handler) RegisterBook(c *gin.Context) {
	var req request.RegisterBookRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
	}
	book, err := h.service.Book.CreateBook(context.Background(), req)
	//log.Println("created book:", book)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
	}
	c.JSON(http.StatusCreated, book)
}

// RegisterAuthor
// @Summary      RegisterAuthor
// @Description  registers an author
// @Tags         author
// @Accept       json
// @Produce      json
// @Param        request body request.RegisterAuthorRequest true "author registered"
// @Success      200 {object} responce.RegisterAuthorResponse
// @Failure      400
// @Failure      500
// @Router       /register-author [POST]
func (h Handler) RegisterAuthor(c *gin.Context) {
	var req request.RegisterAuthorRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
	}
	//log.Println("coming request:", req)
	author, err := h.service.Book.CreateAuthor(context.Background(), req)
	//log.Println(author)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
	}
	c.JSON(http.StatusCreated, author)
}

// RegisterCategory
// @Summary      RegisterCategory
// @Description  registers a category
// @Tags         category
// @Accept       json
// @Produce      json
// @Param        request body request.RegisterCategoryRequest true "category added"
// @Success      200 {object} responce.RegisterCategoryResponse
// @Failure      400
// @Failure      500
// @Router       /register-category [POST]
func (h Handler) RegisterCategory(c *gin.Context) {
	var req request.RegisterCategoryRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err})
	}
	//log.Println("category request:", req)
	category, err := h.service.Book.CreateCategory(context.Background(), req)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err})
	}
	c.JSON(http.StatusCreated, category)
}

// GetAllBooks
// @Summary      GetAllBooks
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Success      200 {object} []responce.RegisterBookResponce
// @Failure      400
// @Failure      500
// @Router       /get-books [GET]
func (h Handler) GetAllBooks(c *gin.Context) {
	books, err := h.service.Book.GetAllBooks(context.Background())
	if err != nil {
		c.JSON(500, gin.H{
			"error1": err.Error(),
		})
		return
	}
	c.JSON(200, books)
}

// DeleteBook
// @Summary      DeleteBook
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        id path string true "DeleteBook"
// @Success      200
// @Failure      400
// @Failure      500
// @Router       /delete-book/{id} [DELETE]
func (h Handler) DeleteBook(c *gin.Context) {
	id := c.Param("id")
	if err := h.service.Book.DeleteBook(context.Background(), id); err != nil {
		c.JSON(500, gin.H{
			"Error:": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"status:": "deleted",
	})
}

// UpdateBook
// @Summary      UpdateBook
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        request body request.UpdateBookRequest true "UpdateBook"
// @Success      200 {object} responce.RegisterBookResponce
// @Failure      400
// @Failure      500
// @Router       /update-book [PUT]
func (h Handler) UpdateBook(c *gin.Context) {
	var requestBook request.UpdateBookRequest
	if err := c.ShouldBindJSON(&requestBook); err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	book, err := h.service.Book.UpdateBook(context.Background(), requestBook)
	if err != nil {
		c.JSON(500, gin.H{
			"Error": err.Error(),
		})
		return
	}
	c.JSON(200, book)
}

// GetBook
// @Summary      GetBook
// @Description
// @Tags         book
// @Accept       json
// @Produce      json
// @Param        id path string true "GetBook"
// @Success      200 {object} responce.RegisterBookResponce
// @Failure      400
// @Failure      500
// @Router       /get-book/{id} [GET]
func (h Handler) GetBook(c *gin.Context) {
	id := c.Param("id")
	b, err := h.service.Book.GetBook(context.Background(), id)
	if err != nil {
		c.JSON(500, gin.H{
			"Error": err.Error(),
		})
		return
	}
	c.JSON(200, b)
}
