package responce

type RegisterBookResponce struct {
	Id       string `json:"id"`
	Title    string `json:"title"`
	Author   string `json:"author"`
	Category string `json:"category"`
}

type RegisterAuthorResponse struct {
	Id        string `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}
type RegisterCategoryResponse struct {
	ID   string `json:"id"`
	Kind string `json:"kind"`
}
