package grpc

import (
	"api-gateway/protos"
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func NewBookServiceClient(ctx context.Context, url string) (protos.BookServiceClient, error) {
	conn, err := grpc.DialContext(
		ctx, url, grpc.WithBlock(), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}
	return protos.NewBookServiceClient(conn), nil
}
