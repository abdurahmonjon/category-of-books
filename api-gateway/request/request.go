package request

type UpdateBookRequest struct {
	Id       string `json:"id"`
	Title    string `json:"title"`
	Author   string `json:"author"`
	Category string `json:"category"`
}
type RegisterBookRequest struct {
	Title    string `json:"title"`
	Author   string `json:"author"`
	Category string `json:"category"`
}
type RegisterAuthorRequest struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type RegisterCategoryRequest struct {
	Kind string `json:"kind"`
}
